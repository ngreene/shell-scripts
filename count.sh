#!/usr/bin/env zsh

# count.sh - Prints a sorted list of file size based on byte, line, or word 
# count. 

# 2021 Nicholas Greene

usage()
{
    echo "Usage: count <-c|-l|-w> <FILES>"
    exit 2
}

set_variable()
{
    local varname=$1
    shift
    if [ -z "${(P)varname}" ]; then
        eval "$varname=\"$@\""
    else
        echo "Error: $varname already set"
        usage
    fi
}

unset OPT

if (( $# == 0 )); then
    usage
fi

while getopts 'clw' ch
do 
    case $ch in
        c) set_variable OPT BYTE ;;
        l) set_variable OPT LINE ;;
        w) set_variable OPT WORD ;;
    esac
done
shift $((OPTIND-1))

opt=""
case $OPT in
    BYTE) opt='c' ;;
    LINE) opt='l' ;;
    WORD) opt='w' ;;
esac

if [ -z "$opt" ]; then 
    usage
fi

for FILE in $@; do echo $(cat $FILE | wc -$opt) $FILE; done | sort -n
