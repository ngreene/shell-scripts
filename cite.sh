#!/usr/bin/env zsh

# cite.sh - renames pdf files for research articles in the way I like because
# I am pedantic.

# 2021 Nicholas Greene

if (( $# != 5 )); then 
    echo "usage: cite [input file] [primary author] [year] [title] [number of citations]"
    exit 0
fi 

inputfile=$1
filename="$2 et al. ($3) - $4 (cit. $5).pdf"
mv $inputfile $filename
echo "\"$inputfile\" renamed \"$filename\""

