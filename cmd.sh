#!/usr/bin/env zsh

# cmd.sh - Makes a script into a custom terminal command. This script is
# superfly. It changes the permissions of a script to be executable, and then 
# it creates a symbolic link for that script in /usr/local/bin (or wherever else
# you specify). The command will be the name of the script without the file
# extension (e.g., my_cool_command.py -> my_cool_command).

usage()
{
    echo "Usage: symlink [-p OUT_PATH] <IN_FILE>"
    exit 2
}

if (( $# == 0 )); then
    usage
fi

in_path=$(pwd)
out_path="/usr/local/bin"

while getopts 'p:?h' ch
do 
    case $ch in
        p) out_path="$OPTARG" ;; 
        ?|h) usage ;;
    esac
done
shift $((OPTIND-1))

initial=$(ls -l $@ | cut -d " " -f1)
chmod 755 $@
final=$(ls -l $@ | cut -d " " -f1)

echo "\nPermissions for $@ changed from $initial to $final\n"

name=$(echo "$@" | cut -f 1 -d '.')
ln -s ${in_path}/$@ ${out_path}/$name
echo "\nSymbolic link complete:\n  File: ${in_path}/$@\n  Link: ${out_path}/$name\n"
