#!/usr/bin/env zsh

# see_bin.sh - Prints all 'binaries'/executables.

# 2021 Nicholas Greene

delim="\n\n=====================================================================\n"
echo $delim
echo "PATH /usr/bin\n"
ls /usr/bin
echo $delim
echo "PATH /usr/sbin\n"
ls /usr/sbin
echo $delim
echo "PATH /usr/local/bin\n"
ls /usr/local/bin 
echo $delim
echo "PATH /usr/local/sbin\n"
ls /usr/local/sbin
echo $delim
echo "PATH /opt/local/bin\n"
ls /opt/local/bin
echo $delim
echo "PATH /opt/local/sbin\n"
ls /opt/local/sbin
echo $delim
echo "Homebrew packages\n"
brew list
echo $delim

